### Exercice ###

Exercice de création de l'API REST en PHP

En bonus création d'une IHM en Angular2 pour utilisation API

IHM GET Liste : OK

IHM POST Ajout utilisateur : En cours

IHM DELETE Suppression utilisateur : -

### Contenu ###
Le repository est scindé en 2 dossiers 

Imposé :

Un dossier nommé API regroupant l'API et ses web services (list, création, delete) d'utilisateur (nom et prenom)

Bonus : 

un dossier nommé INTERFACE regroupant l'IHM angular. Etant en apprentissage la dessus une partie est opérationnelle seulement pour le moment (liste des Users).