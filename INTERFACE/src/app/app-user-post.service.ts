import {HttpModule, Http,Response} from '@angular/http';
import {Injectable} from '@angular/core';
import 'rxjs/add/operator/map'
import { Iuser } from './user';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';


@Injectable()
export class AppUserPostService {
    http: Http;
    returnPostUserStatus:Object = [];
    posts_Url: string = 'http://localhost/pressinnov-pack/API/public/user/add/';

    constructor(public _http: Http) {
       this.http = _http;
    }

    postUser(user:Iuser) {    
       return this.http.post(this.posts_Url, user, {})
       .map(res =>  res.json());     	    
    }


    
}