import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { AppListComponent } from './app-list.component';
import { AppUserPostComponent } from './app-user-post.component';

@NgModule({
  declarations: [
    AppComponent,
    AppListComponent,
    AppUserPostComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
