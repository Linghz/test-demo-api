import { Injectable } from '@angular/core';
import { Iuser } from './user';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';


@Injectable()
export class AppListService {
    private _productUrl ='http://localhost/pressinnov-pack/API/public/users/'; // pour exemple c'est un fichier local
    constructor(private _http: Http) {

    }
    getUsers(): Observable<Iuser[]> { // return Iuser array
        return this._http.get(this._productUrl)
                    .map((response: Response) => <Iuser[]>response.json()) // on A un retour en Response qu'on veut transformer en Array
                    .do(data => console.log('All: '+ JSON.stringify(data)))
                    .catch(this.handleError);
    }
    private handleError(error: Response){
        console.log(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}