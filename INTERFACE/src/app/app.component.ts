import { Component } from '@angular/core';
import { AppListService } from './app-list.service';
import { AppUserPostService } from './app-user-post.service';
import { Iuser } from './user';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AppListService, AppUserPostService]
})
export class AppComponent {
  users : Iuser[];
  title = "Interface Press'Innov";
  errorMessage : string;

  constructor(private _appListService: AppListService, private _appUserPostService: AppUserPostService  ){

  }
}

