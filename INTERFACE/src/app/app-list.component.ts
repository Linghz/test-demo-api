import { Component, OnInit } from '@angular/core';
import { Iuser } from './user';
import { AppListService } from './app-list.service';

@Component({
    selector: 'app-list',
    templateUrl : './app-list.component.html'
})
export class AppListComponent implements OnInit{
    users : Iuser[];
    errorMessage : string;

    constructor(private _appListService: AppListService ){

    }
    ngOnInit(): void{
        this._appListService
        .getUsers().subscribe(users => this.users = users, // recup data http
        error => this.errorMessage = <any>error); 
                    
    }

}