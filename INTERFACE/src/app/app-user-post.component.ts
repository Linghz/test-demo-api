import { Component, Input } from '@angular/core';
import { AppUserPostService } from './app-user-post.service'
import { Iuser } from './user'
@Component({
    // moduleId: module.id,
    selector: 'app-user-post',
    templateUrl: './app-user-post.component.html',
    providers: [AppUserPostService]
})
export class AppUserPostComponent {

    constructor(private _appUserPostService: AppUserPostService) { }
 
    @Input() user:Iuser;
    nom: string;
    prenom:string;

    responseStatus:Object= [];
    status:boolean ;
  

    //temporairement désactivé car erreur sur ngModel

    // submitPost()
    // {        
    //     // console.log("submit Post click happend " + this.user.prenom + " " + this.user.nom)
    //     this._appUserPostService.postUser(this.user).subscribe(
    //        data => console.log(this.responseStatus = data),
    //        err => console.log(err),
    //        () => console.log('Request Completed')
    //     ); 

    //     this.status = true;       
    // }

}