import { PressinnovInterfaceDemoPage } from './app.po';

describe('pressinnov-interface-demo App', () => {
  let page: PressinnovInterfaceDemoPage;

  beforeEach(() => {
    page = new PressinnovInterfaceDemoPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
