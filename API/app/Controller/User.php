<?php
namespace App\Controller;

class User
{
    private $id;
    private $nom;
    private $prenom;


    public function getId()
    {
        return $this->id;
    }


    public function getNom()
    {
        return $this->nom;
    }


    public function getPrenom()
    {
        return $this->prenom;
    }


    public function setId($id)
    {
        // L'identifiant sera, quoi qu'il arrive, un nombre entier.
        $this->id = (int) $id;
    }


    public function setNom($nom)
    {
        // On vérifie qu'il s'agit bien d'une chaîne de caractères.
        if (is_string($nom))
        {
            $this->nom = $nom;
        }
    }

    public function setPrenom($prenom)
    {
        // On vérifie qu'il s'agit bien d'une chaîne de caractères.
        if (is_string($prenom))
        {
            $this->prenom = $prenom;
        }
    }

}


