<?php

namespace App\Controller;

use PDO;

class UserManager
{
    private $_db; // variable instantiation PDO

    public function __construct($db)
    {
        $this->_db = $db;
    }

    public function userList()
    {
        $sql = "SELECT * FROM user";
        $query = $this->_db->query($sql);
        $dataretour= [];
        while ($donnees = $query->fetch(PDO::FETCH_ASSOC))
        {
            $dataretour[]= array('prenom' => $donnees['prenom'], 'nom'=> $donnees['nom']);
        }

        return $dataretour;
    }

    public function userAdd(User $user)
    {
        $sql = "INSERT INTO user(nom, prenom) VALUES (:nom, :prenom)";
        $query = $this->_db->prepare($sql);
        $query->bindValue(':nom', $user->getNom() );
        $query->bindValue(':prenom', $user->getPrenom() );
        $query->execute();
    }

    public function userDelete(User $user)
    {
        $sql = "DELETE FROM user WHERE id = :id";
        $query = $this->_db->prepare($sql);
        $query->bindValue(':id', $user->getId());
        $query->execute();
    }
}