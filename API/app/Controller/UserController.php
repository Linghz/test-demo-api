<?php

namespace App\Controller;

use App\Controller\User;
use App\Controller\UserManager;
use PDO;
use Slim\Views\Twig;


class UserController
{

    protected $db;

    public function __construct()
    {
        $this->db = new PDO("sqlite:../db/base.db3");
    }

    public function userAdd($request, $response, $args){
        if($request->isGet() || $request->isPost())
        {
            // if(!is_null($request->getParam('nom')) && !is_null($request->getParam('prenom')))
            if(null !== $request->getParam('nom')  && null !== $request->getParam('prenom') && $request->getParam('prenom') !='' && $request->getParam('nom') !='')
            {
                //récupération des données du formulaire
                $nom = strip_tags($request->getParam('nom'));
                $prenom = strip_tags($request->getParam('prenom'));

                //hydratation de l'user
                $user = new User();
                $user->setNom($nom);
                $user->setPrenom($prenom);

                //création en base via le manager
                $userManager = new UserManager($this->db);
                $userManager->userAdd($user);

                $dataretour = array('status' => 'OK');
            }
            else
            {
                $dataretour = array('status' => 'ERROR', 'NOM' => $request->getParam('nom'), 'PRENOM' => $request->getParam('prenom') );

            }
            return $response->withJSON($dataretour);
        }
    }

    public function userDelete($request, $response, $args)
    {
        $id = strip_tags($args["id"]);
        if ($request->isDelete())
        {
            //hydratation de l'user
            $user = new User();
            $user->setId($id);

            //suppression en bdd via le UserManager
            $userManager = new UserManager($this->db);
            $userManager->userDelete($user);

            $dataretour = array('status' => 'OK');
        }
        else
        {
            $dataretour = array('status' => 'ERROR');
        }
        return $response->withJSON($dataretour);
    }

    public function userList($request, $response, $args) // Liste transmise à l'interface
    {
        if($request->isGet() || $request->isPost())
        {
            $userManager = new UserManager($this->db);
            return $response->withJSON($userManager->userList());
        }
        else
        {
            $dataretour = array('status' => 'ERROR');
            return $response->withJSON($dataretour);
        }
    }
}