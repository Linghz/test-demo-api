<?php
// Routes


//$app->get('/users/', function ($request, $response, $args) {
////    return $this->renderer->render($response, 'test.phtml', $args);
//    $data = array('name' => 'Bob', 'age' => 40);
//    return $response->withJSON($data);
//});

//Liste des Users
$app->get('/users/', 'App\Controller\UserController:userList');
//Ajouter un User

$app->group('/user/add/', function(){
    $this->map(['GET', 'POST'], '','App\Controller\UserController:userAdd');
});

//Delete un User
$app->group('/user/{id}', function(){
    $this->map(['DELETE'], '','App\Controller\UserController:userDelete');
});




